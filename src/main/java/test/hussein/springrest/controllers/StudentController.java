package test.hussein.springrest.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import test.hussein.springrest.Exceptions.StudentNotFoundException;
import test.hussein.springrest.Models.Student;
import test.hussein.springrest.Repositories.StudentRepository;

@RestController
@RequestMapping("/api/v1")
public class StudentController {

	@Autowired
	private StudentRepository studentRepo;
	
	@GetMapping("/students")
	public List<Student> getStudents(){
		return studentRepo.findAll();
	}
	
	@GetMapping("/students/{id}")
	public Student getStudent(@PathVariable("id") Long id) {
		return studentRepo.findById(id).orElseThrow(()-> new StudentNotFoundException("Student with id" + id + " Not Found"));
	}
	
	@PostMapping("/students")
	public void addStudent(@Valid @RequestBody Student student) {
		studentRepo.save(student);
	}
	
	@PutMapping("/student/{id}")
	public Student updateStudent(@RequestBody @Valid Student student) {
		Student studentObj = studentRepo.findById(student.getId()).orElseThrow(()-> new StudentNotFoundException("Student with id" + student.getId() + " Not Found"));
		return studentRepo.save(studentObj);
	}
	
	@DeleteMapping("/student/{id}")
	public void deleteStudent(@PathVariable("id") Long id) {
		studentRepo.findById(id).orElseThrow(()-> new StudentNotFoundException("Student with id" + id + " Not Found"));
		studentRepo.deleteById(id);
	}
	
}
