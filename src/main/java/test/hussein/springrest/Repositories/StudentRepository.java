package test.hussein.springrest.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import test.hussein.springrest.Models.Student;

public interface StudentRepository extends JpaRepository<Student, Long> {

}
